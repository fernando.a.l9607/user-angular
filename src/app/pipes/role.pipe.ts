import { Pipe, PipeTransform } from '@angular/core';
import { UsersConsumerService } from '../components/users/users-consumer.service';

@Pipe({
  name: 'role'
})
export class RolePipe implements PipeTransform {
  transform(value: any, ...args: any): any {

    let valueFin: string;
      args[0].filter(rol => {
        if(rol.id === value){
          valueFin=rol.position
          return valueFin
        }
      })
      return valueFin
  }

}

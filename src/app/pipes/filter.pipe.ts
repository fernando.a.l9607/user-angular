import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, ...arg: any[]): any {
    if (!value || !value) {
      return value;
    }
    return value.filter(item => item.email.indexOf(arg) !== -1 || item.name.indexOf(arg) !== -1);

  }

}

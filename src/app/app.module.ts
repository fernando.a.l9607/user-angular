import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { UsersComponent } from './components/users/users.component'


import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'
import { from } from 'rxjs';
import { FilterPipe } from './pipes/filter.pipe';
import { FormsModule } from '@angular/forms';
import { RolePipe } from './pipes/role.pipe';
import { ListUserComponent } from './components/users/list-user/list-user/list-user.component';
import { ModalUserAddComponent } from './components/modals/modals-users/modal-user-add/modal-user-add.component';
@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    FilterPipe,
    RolePipe,
    ListUserComponent,
    ModalUserAddComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

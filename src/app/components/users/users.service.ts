import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  removeUserNext: Subject<Object> = new Subject<Object>();
  addUserUserNext: Subject<Object> = new Subject<Object>();

  saveUserNext: Subject<Object> = new Subject<Object>();


  constructor() { }

  removeUser(data) {
    this.removeUserNext.next(data);
  }

  addUserUser(data) {
    this.addUserUserNext.next(data);
  }

  saveUser(data) {
    this.saveUserNext.next(data);
  }
}

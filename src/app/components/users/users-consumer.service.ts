import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class UsersConsumerService {
  info:any={};
  loaded:boolean = false;

  constructor(private http:HttpClient) { 
  }

  getUsers(): Promise<Object> {
    return new Promise((resolve, reject) => {
      this.http.get("../../../assets/response-json/users.json").subscribe((data: Object) => {
        resolve(data["users"]);
      }, (error) => {
        reject(error);
      });

    });
  }

  getRoles(): Promise<Object> {
    return new Promise((resolve, reject) => {
      this.http.get("../../../assets/response-json/roles.json").subscribe((data: Object) => {
        resolve(data["roles"]);
      }, (error) => {
        reject(error);
      });

    });
  }


}

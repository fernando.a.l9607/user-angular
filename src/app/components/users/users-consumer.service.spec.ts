import { TestBed } from '@angular/core/testing';

import { UsersConsumerService } from './users-consumer.service';

describe('UsersConsumerService', () => {
  let service: UsersConsumerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsersConsumerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

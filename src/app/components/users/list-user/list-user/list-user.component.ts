import { Component, OnInit, Input } from '@angular/core';
import { IUserRol, IUser } from 'src/app/models/user.interface';
import { UsersService } from '../../users.service';

@Component({
  selector: '[app-list-user]',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {
  @Input("user") user: IUser;
  @Input("userRol") userRol: IUserRol;

  constructor(
    public usersService: UsersService
  ) { 
  }

  ngOnInit(): void {

  }

  removeUser(idUser){
    this.usersService.removeUser(idUser);
  }

  desactivUser(check){
    this.user.active=check;
  }

  editUser(){
    this.usersService.addUserUser(this.user);
  }

}

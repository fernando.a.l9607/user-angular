import { Component, OnInit } from '@angular/core';
import { UsersConsumerService } from 'src/app/components/users/users-consumer.service';
import { IUser, IUserRol } from 'src/app/models/user.interface';
import { UsersService } from 'src/app/components/users/users.service'

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  data:IUser[];
  dataRole:IUserRol;
  loadedRol:Boolean = false;
  filterUser = '';
  constructor(
    public usersConsumerService: UsersConsumerService,
    public usersService: UsersService
  ) { }

  ngOnInit(): void {
    this.usersService.removeUserNext.subscribe((idUser) => {
      this.removeUserArr(idUser);
    })
    this.usersService.saveUserNext.subscribe((userData) =>{
      this.saveUserNew(userData);
    })
    this.getUser();
  }

  saveUserNew(userData){
    this.loadedRol = false;
    if(userData['active'] !== null){
      const userDataInfo = {
        picture: userData.picture,
        name: userData.name,
        fathersLastName: userData.fathersLastName,
        mothersLastName: userData.mothersLastName,
        email: userData.email,
        roleId: userData.roleId,
        active: true,
        idUser: this.data.length + 1,
      }
      setTimeout(() => {
        this.data.push(userDataInfo);
        this.loadedRol = true;
      }, 100);
      this.data = this.data;


    }else{
      this.data.filter((user,index) => {
        if(user.idUser === userData.idUser){
          setTimeout(() => {
            this.data[index] = userData;
            this.loadedRol = true;
          }, 100);
          this.data = this.data;
        }
      })
    }
  }
  
   compare(a, b) {

    const bandA = a.name.toUpperCase();
    const bandB = b.name.toUpperCase();
  
    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison;
  }

  removeUserArr(idUser){
    this.loadedRol = false;
    this.data.filter((user,index) => {
      if(user.idUser === idUser){
        setTimeout(() => {
          delete this.data[index];
          this.loadedRol = true;
        }, 100);
        this.data = this.data;
      }
    })
  }

  sortUserAZ(){
    this.loadedRol = false;
    setTimeout(() => {
      this.data = this.data.sort(this.compare);
      this.loadedRol = true;
    }, 100);
  }

  getUser(){
    this.data = null;
    this.usersConsumerService.getUsers().then((users:IUser[]) =>{
      this.data = users;
      this.getRol();
      this.data.map((user,index) => {
        this.data[index].idUser = index+1;
      })
    });
  }

  getRol(){
    this.dataRole = null;
    this.loadedRol = false;
    this.usersConsumerService.getRoles().then((users:IUserRol) =>{
      this.loadedRol = true;
      this.dataRole = users;
    })
  }

  addUser(){
    const data = {}
    this.usersService.addUserUser(data);
  }


}

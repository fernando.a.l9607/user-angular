import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/components/users/users.service';
import { IUser } from 'src/app/models/user.interface';

@Component({
  selector: 'app-modal-user-add',
  templateUrl: './modal-user-add.component.html',
  styleUrls: ['./modal-user-add.component.scss']
})
export class ModalUserAddComponent implements OnInit {
  dataUser:IUser;
  temporalUser: IUser;
  loader: boolean = false;
  constructor(
    public usersService: UsersService,
  ) { }

  ngOnInit(): void {
    this.usersService.addUserUserNext.subscribe((data:IUser) => {
      if(data !== null){
        setTimeout(() => {
          this.dataUser = data
          this.temporalUser = data;
          this.loader = true;
        }, 100);      
      }
      this.openModalUser();
    })
  }

  saveUser(){
    this.usersService.saveUser(this.dataUser)
    setTimeout(() => {
      this.temporalUser = this.dataUser;
      this.closeModalUser();
    }, 200);
  }

  openModalUser(){
    document.getElementById('overlay').classList.add('is-visible');
    document.getElementById('modal').classList.add('is-visible');
  }

  cancelUser(){
    this.dataUser = this.temporalUser;
    this.closeModalUser();
  }
  
  closeModalUser(){
    document.getElementById('overlay').classList.remove('is-visible');
    document.getElementById('modal').classList.remove('is-visible');
  }

}

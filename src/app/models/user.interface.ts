export interface IUser{
    picture: string;
    name: string;
    fathersLastName: string;
    mothersLastName: string;
    email: string;
    roleId: number;
    active?: boolean;
    idUser?: number;
}

export interface IUserRol{
    id: number;
    position: string;
}